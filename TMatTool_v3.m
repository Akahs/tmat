function varargout = TMatTool_v3(varargin)
% TMATTOOL_V3 MATLAB code for TMatTool_v3.fig
%      TMATTOOL_V3, by itself, creates a new TMATTOOL_V3 or raises the existing
%      singleton*.
%
%      H = TMATTOOL_V3 returns the handle to a new TMATTOOL_V3 or the handle to
%      the existing singleton*.
%
%      TMATTOOL_V3('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TMATTOOL_V3.M with the given input arguments.
%
%      TMATTOOL_V3('Property','Value',...) creates a new TMATTOOL_V3 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TMatTool_v3_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TMatTool_v3_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TMatTool_v3

% Last Modified by GUIDE v2.5 12-Jan-2016 12:00:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TMatTool_v3_OpeningFcn, ...
                   'gui_OutputFcn',  @TMatTool_v3_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TMatTool_v3 is made visible.
function TMatTool_v3_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TMatTool_v3 (see VARARGIN)

% Choose default command line output for TMatTool_v3
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TMatTool_v3 wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global polarization xmin xmax title ymin ymax;
set(handles.edit1,'String',0);
set(handles.edit2,'String',675);
set(handles.edit3,'String',0);
set(handles.edit4,'String',9);
set(handles.edit5,'String',9);
set(handles.edit6,'String',50);
set(handles.Loaded,'Checked','off');
set(handles.slider1,'Value',0);
set(handles.Unpolarized,'Checked','on')
polarization=0;
xmin=400;
xmax=1200;
ymin=0;
ymax=1;
TMatF_v6(handles);


% --- Outputs from this function are returned to the command line.
function varargout = TMatTool_v3_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
set(handles.slider1,'Value',str2double(get(handles.edit1,'String')));
damping_GaN(handles,'film');
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.edit1,'String',num2str(get(handles.slider1,'Value'),4));
n=get(handles.slider1,'Value');
gamma=1.44e-5*n^0.4;
set(handles.slider2,'Value',gamma);
set(handles.edit3,'String',num2str(gamma));
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
gamma=get(handles.slider2,'Value');
set(handles.edit3,'String',num2str(gamma,4));
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
gamma=str2double(get(handles.edit3,'String'));
set(handles.slider2,'Value',gamma);
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Load_Callback(hObject, eventdata, handles)
% hObject    handle to Load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global spectrum;
dlgtitle='Load data';
prompt='Please type in the filename (.mat file only, containing variable called "spectrum"):';
str=inputdlg(prompt,dlgtitle);
if isempty(str)
    return;
end
str=str{:};
if exist(str,'file')
    load(str);
    if isempty(spectrum)
        msgbox('Format error!');
        clear;
    end
else
    msgbox('File not exist!');
end
    


% --------------------------------------------------------------------
function tagPlot_Callback(hObject, eventdata, handles)
% hObject    handle to tagPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Loaded_Callback(hObject, eventdata, handles)
% hObject    handle to Loaded (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global spectrum;
str=get(handles.Loaded,'Checked');
if strcmp(str,'on')
    set(handles.Loaded,'Checked','off');
    TMatF_v6(handles);
else
    if ~isempty(spectrum);
        set(handles.Loaded,'Checked','on');
        TMatF_v6(handles);
    else
        msgbox('Please load data first!');
    end
end


% --------------------------------------------------------------------
function Unload_Callback(hObject, eventdata, handles)
% hObject    handle to Unload (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Loaded,'Checked','off');
TMatF_v6(handles)


% --------------------------------------------------------------------
function title_Callback(hObject, eventdata, handles)
% hObject    handle to title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
str=get(handles.title,'Checked');
if strcmp(str,'on')
    set(handles.title,'Checked','off');
    pTitle(handles);
else
    set(handles.title,'Checked','on');
    pTitle(handles);
end


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Polarization_Callback(hObject, eventdata, handles)
% hObject    handle to Polarization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function TE_Callback(hObject, eventdata, handles)
% hObject    handle to TE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global polarization;
polarization=1;
set(handles.TE,'Checked','on');
set(handles.TM,'Checked','off');
set(handles.Unpolarized,'Checked','off');
TMatF_v6(handles);



% --------------------------------------------------------------------
function TM_Callback(hObject, eventdata, handles)
% hObject    handle to TM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global polarization;
polarization=2;
set(handles.TM,'Checked','on');
set(handles.Unpolarized,'Checked','off');
set(handles.TE,'Checked','off');
TMatF_v6(handles);



% --------------------------------------------------------------------
function Unpolarized_Callback(hObject, eventdata, handles)
% hObject    handle to Unpolarized (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global polarization;
polarization=0;
set(handles.Unpolarized,'Checked','on');
set(handles.TE,'Checked','off');
set(handles.TM,'Checked','off');
TMatF_v6(handles);


% --------------------------------------------------------------------
function xlim_Callback(hObject, eventdata, handles)
% hObject    handle to xlim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global xmin xmax
temp=inputdlg({'xmin','xmax'});
if ~isempty(temp)
    xmin=str2double(temp(1));
    xmax=str2double(temp(2));
    TMatF_v6(handles);
end


% --------------------------------------------------------------------
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dlgtitle='Export plot';
prompt='Set file name:';
default = {'reflectivity.txt'};
numlines = 1;
expname=inputdlg(prompt,dlgtitle,numlines,default);
if ~isempty(expname)
    expname=expname{:};
    ExpF(expname,handles);
end


% --------------------------------------------------------------------
function grid_Callback(hObject, eventdata, handles)
% hObject    handle to grid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(hObject,'Checked'), 'on')
    set(hObject,'Checked','off')
    grid(handles.axes1,'off')
else
    set(hObject,'Check','on')
    grid(handles.axes1,'on')
end


% --- Executes on selection change in pop1.
function pop1_Callback(hObject, eventdata, handles)
% hObject    handle to pop1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop1
% set(handles.edit2,'String','1000');
id = get(hObject,'Value');
if id >= 2
    set(handles.edit1,'Enable','off');
    set(handles.slider1,'Enable','off');
    % disable plasmon damping
    set(handles.edit3,'Enable','off');
    set(handles.slider2,'Enable','off');
else
    set(handles.edit1,'Enable','on');
    set(handles.edit1,'String','0');
    set(handles.slider1,'Enable','on');
    set(handles.slider1,'Value',0);
    set(handles.edit3,'Enable','on');
    set(handles.slider2,'Enable','on');
end
if id == 5
    set(handles.edit4,'Enable','off');
end
    
DampingIni(handles,'film');
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function pop1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pop2.
function pop2_Callback(hObject, eventdata, handles)
% hObject    handle to pop2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop2
DampingIni(handles,'substrate');
id = get(hObject,'Value');
if id == 5
    set(handles.edit5,'Enable','off');
end
TMatF_v6(handles);


% --- Executes during object creation, after setting all properties.
function pop2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function ylim_Callback(hObject, eventdata, handles)
% hObject    handle to ylim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ymin ymax;
temp=inputdlg({'ymin','ymax'});
if ~isempty(temp)
    ymin=str2double(temp(1));
    ymax=str2double(temp(2));
%     TMatF_v6(handles);
%     plot(handles.axes1,omega,R);
%     ylim([ymin ymax]);
end
TMatPlot(handles);

