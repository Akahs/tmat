N=500;
n=logspace(17,20,N); %doping
q=4.803e-10;
eps_inf=5.35;
c=3e10; %cm/s
m=0.2*9.1e-28; %effective mass in g
omega_LO=746;
omega_TO=556;
myfun=@(f,doping) GaN14eps_doped;
L_p=zeros(1,N);
for k=1:N
    omega_p=sqrt(4*pi*n*q^2/(eps_inf*m));
    omega_p=omega_p/(2*pi*c);
    doping=n(k);
    fun=@(f) myfun(f,doping);
    x0=max([omega_TO,omega_p);
    L_p(k)=fzero(fun,x0);
end
h=figure(1);
fsz=14;
plot(omega,L_p);
grid on;
set(gca,'FontSize',fsz);
xlabel('Doping(cm^{-3})');
ylabel('Frequency(cm^{-1})');
% title('Dielectric function of GaN with 7e18 doping');
set(h,'PaperPosition',[0.25,2.5,8,5]);
% print(h,'coupled_eps','-djpeg','-r300');