function damping_Air(handles, part)
    if strcmp(part,'film')
        % set phonon damping
        set(handles.edit4,'String',0);
    elseif strcmp(part,'substrate')
        set(handles.edit5,'String',0);
    else
        warndlg('Internal error: unknown part name');
    end
end