% Author: Kaijun Feng
% Date: 7/30/2015
% Notes: this is the modified version of epsInP to accommodate the TMat
% tool. Lorentzian model was used.
% Ref: Solid State Comm. vol 136, pp. 404 (2005)
function eps=epsInP_GUI(omega, handles, part)
omega_LO = 345; % 1/cm
omega_TO = 304;
eps_inf = 9.61;
if strcmp(part,'film')
    gamma=str2double(get(handles.edit4,'String'));
else
    gamma = str2double(get(handles.edit5,'String'));
end
% Lorentzian model
eps = eps_inf*(1+(omega_LO^2-omega_TO^2)./(omega_TO^2-omega.^2+1i*omega*gamma));
end