% Author: Kaijun Feng 
% Date: June 15th, 2015
% Note: This model use transfer matrix method with isotopic approximation.
% The film and substrate materials can be chosen from the popup menus in
% the GUI.
function TMatF_v6(handles)
global spectrum xmin xmax;
global omega R polarization; % 0=unplarized, 1=TE, 2=TM
%% parameter definition
temp=str2double(get(handles.edit2,'String'));
d_film=temp*1e-7; %cm
omega=xmin:xmax; %cm-1
c=3e10;%cm/s
% f=c*omega;%1/s
N=length(omega);
R_TE=zeros(1,N); %reflectance TE
R_TM=zeros(1,N); %reflectance TM
angle=str2double(get(handles.edit6,'String'));
theta_i=angle*pi/180; % angle in radius
% lw=2; %line width
% fsz=18; %FontSize
hwb = waitbar(0,'Calculating...','Name','TMat rocks');

%% Calculating reflectivity
funcs = {'epsGaN14_doped_GUI','epsSiC_GUI','epsInP_GUI','epsAir_GUI','epsSilica_GUI'};
id_film = get(handles.pop1,'Value');
id_sub = get(handles.pop2,'Value');
% TE
if (polarization==0) || (polarization==1)
    for j=1:N
        eps_film = feval(funcs{id_film},omega(j),handles,'film');
        n_film=sqrt(eps_film);
        eps_sub=feval(funcs{id_sub},omega(j),handles,'substrate');
        n_sub=sqrt(eps_sub);
        n0=1;
        theta_film=asin(sin(theta_i)/n_film);
        theta_sub=asin(sin(theta_film)*n_film/n_sub);
        Z0=377; %Ohm
        Y0=1/Z0*n0*cos(theta_i); %S
        Ys=1/Z0*n_sub*cos(theta_sub);
        Y_film=1/Z0*n_film*cos(theta_film);
        k0=2*pi*omega(j); %rad/cm
        k0h_film=k0*n_film*d_film*cos(theta_film);
        M=[cos(k0h_film) (1i*sin(k0h_film))/Y_film;
            Y_film*1i*sin(k0h_film) cos(k0h_film)];
        r=(Y0*M(1,1)+Y0*Ys*M(1,2)-M(2,1)-Ys*M(2,2))/(Y0*M(1,1)+Y0*Ys*M(1,2)+M(2,1)+Ys*M(2,2));
        R_TE(j)=abs(r)^2;
        if (polarization == 0)
            waitbar(j/(2*N),hwb);
        else
            waitbar(j/N,hwb);
        end
    end
end

%TM
if (polarization==0) || (polarization==2)
    for j=1:N
        eps_film = feval(funcs{id_film},omega(j),handles,'film');
        n_film=sqrt(eps_film);
        eps_sub=feval(funcs{id_sub},omega(j),handles,'substrate');
        n_sub=sqrt(eps_sub);
        n0=1;
        theta_film=asin(sin(theta_i)/n_film);
        theta_sub=asin(sin(theta_i)/n_sub);
        Z0=377; %Ohm
        Y0=1/Z0*n0/cos(theta_i); %S
        Ys=1/Z0*n_sub/cos(theta_sub); 
        Y_film=1/Z0*n_film/cos(theta_film);
        k0=2*pi*omega(j); %rad/cm
        k0h_film=k0*n_film*d_film*cos(theta_film);
        M=[cos(k0h_film) (1i*sin(k0h_film))/Y_film;
            Y_film*1i*sin(k0h_film) cos(k0h_film)];
        r=(Y0*M(1,1)+Y0*Ys*M(1,2)-M(2,1)-Ys*M(2,2))/(Y0*M(1,1)+Y0*Ys*M(1,2)+M(2,1)+Ys*M(2,2));
        R_TM(j)=abs(r)^2;
        if (polarization == 0)
            waitbar(0.5 + j/(2*N),hwb);
        else
            waitbar(j/N,hwb);
        end
    end
end
close(hwb);
% TE+TM
if polarization==0
    a=0.5;
    R=a*R_TE+(1-a)*R_TM;
elseif polarization==1
    R=R_TE;
else
    R=R_TM;
end

%% Plot settings
TMatPlot(handles);
% plot(handles.axes1,omega,R,'LineWidth',lw);
% % grid on;
% xlabel('Frequency(cm^{-1})','FontSize',fsz);
% ylabel('Reflectance','FontSize',fsz);
% set(gca,'FontSize',fsz);
% % title('Reflectance of GaN film(transfer matrix, 670nm, 50d)');
% xlim([xmin xmax]);
% % ylim([0 1]);
% % hold on;
% % plot(omega_exp,GaN14_50d,'LineWidth',lw);
% % % hold off;
% 
% % Experiment data
% str=get(handles.Loaded,'Checked');
% if strcmp(str,'on')
%     hold on;
%     plot(spectrum(:,1),spectrum(:,2),'LineWidth',lw);
%     hl=legend('Simulation','Experiment','Location','Best');
%     set(hl,'FontSize',fsz-2);
%     hold off;
% end    

end