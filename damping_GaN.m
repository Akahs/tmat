% Author: Kaijun Feng 
% Date: June 15th, 2015
% Note: This function is used to initialize the damping constants for GaN.
% The varialbe 'part' can be either 'film' or 'substrate'.
function damping_GaN(handles, part)
    if strcmp(part,'film')
        % set plasmon damping
        n=get(handles.slider1,'Value');
        gamma=1.44e-5*n^0.4; % fitting result, using data from literature.
        set(handles.slider2,'Value',gamma);
        set(handles.edit3,'String',num2str(gamma));
        % set phonon damping
        set(handles.edit4,'String','9');
    elseif strcmp(part,'substrate')
        set(handles.edit5,'String',9);
    else
        warndlg('Internal error: unknown part name');
    end
end