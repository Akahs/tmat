% Author: Kaijun Feng 
% Date: July 30th, 2015
% Note: This function is used to initialize the damping constants for InP.
% The varialbe 'part' can be either 'film' or 'substrate'.
function damping_InP(handles,part)
    if strcmp(part,'film')
        % set phonon damping
        set(handles.edit4,'String',3);
    elseif strcmp(part,'substrate')
        set(handles.edit5,'String',3);
    else
        warndlg('Internal error: unknown part name');
    end
end