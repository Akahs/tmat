function TMatPlot(handles)
global xmin xmax ymin ymax omega R;
lw=2; %line width
fsz=18; %FontSize
plot(handles.axes1,omega,R,'LineWidth',lw);
% grid on;
xlabel('Frequency(cm^{-1})','FontSize',fsz);
ylabel('Reflectance','FontSize',fsz);
set(gca,'FontSize',fsz);
% title('Reflectance of GaN film(transfer matrix, 670nm, 50d)');
xlim([xmin xmax]);
ylim([ymin ymax]);
% hold on;
% plot(omega_exp,GaN14_50d,'LineWidth',lw);
% % hold off;

% Experiment data
str=get(handles.Loaded,'Checked');
if strcmp(str,'on')
    hold on;
    plot(spectrum(:,1),spectrum(:,2),'LineWidth',lw);
    hl=legend('Simulation','Experiment','Location','Best');
    set(hl,'FontSize',fsz-2);
    hold off;
end    