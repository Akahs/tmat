% Author: Kaijun Feng 
% Date: Oct. 11th, 2014
% Note: This function is based on the script "Transfer_matrix_v3". This
% model use transfer matrix method with isotopic approximation. SiC was
% used as the substrate since the SiC layer is much thicker than GaN layer.
% SiC is undoped while GaN is doped.
%% parameter definition
d_GaN=670e-7; %cm
d_SiC=350e-4; %cm
omega=400:950; %cm-1
c=3e10;%cm/s
f=c*omega;%1/s
N=length(omega);
R_TE=zeros(1,N); %reflectance TE
R_TM=zeros(1,N); %reflectance TM
theta_i=50*pi/180; %50 degree in rad
Gamma_SiC=4; %cm-1 guess
% plot parameters
width = 10;     % Width in inches
height = 6;    % Height in inches
alw = 0.75;    % AxesLineWidth
lw=2; %line width
fsz=24; %FontSize

%% GaN14
omega_TO_GaN=556; %cm-1
omega_LO_GaN=735; %cm-1
omega_TO_SiC=797; %cm-1
omega_LO_SiC=970; %cm-1
eps_inf_GaN=5.35;
eps_inf_SiC=6.56;
Gamma_GaN=9; %cm-1 guess
for j=1:N
    eps_GaN=eps_inf_GaN*(1+(omega_LO_GaN^2-omega_TO_GaN^2)./(omega_TO_GaN^2-...
        omega(j)^2+1i*omega(j)*Gamma_GaN));
    n_GaN=sqrt(eps_GaN);
    eps_SiC=eps_inf_SiC*(1+(omega_LO_SiC^2-omega_TO_SiC^2)./(omega_TO_SiC^2-...
        omega(j)^2+1i*omega(j)*Gamma_SiC));
    n_SiC=sqrt(eps_SiC);
    n0=1;
    theta_GaN=asin(sin(theta_i)/n_GaN);
    theta_SiC=asin(sin(theta_GaN)*n_GaN/n_SiC);
    Z0=377; %Ohm
    Y0=1/Z0*n0*cos(theta_i); %S
    Ys=1/Z0*n_SiC*cos(theta_SiC);
    Y_GaN=1/Z0*n_GaN*cos(theta_GaN);
    k0=2*pi*omega(j); %rad/cm
    k0h_GaN=k0*n_GaN*d_GaN*cos(theta_GaN);
    M=[cos(k0h_GaN) (1i*sin(k0h_GaN))/Y_GaN;
        Y_GaN*1i*sin(k0h_GaN) cos(k0h_GaN)];
    r=(Y0*M(1,1)+Y0*Ys*M(1,2)-M(2,1)-Ys*M(2,2))/(Y0*M(1,1)+Y0*Ys*M(1,2)+M(2,1)+Ys*M(2,2));
    R_TE(j)=abs(r)^2;
end

%TM
for j=1:N
    eps_GaN=eps_inf_GaN*(1+(omega_LO_GaN^2-omega_TO_GaN^2)/(omega_TO_GaN^2-...
        omega(j)^2+1i*omega(j)*Gamma_GaN));
    n_GaN=sqrt(eps_GaN);
    eps_SiC=eps_inf_SiC*(1+(omega_LO_SiC^2-omega_TO_SiC^2)./(omega_TO_SiC^2-...
        omega(j)^2+1i*omega(j)*Gamma_SiC));
    n_SiC=sqrt(eps_SiC);
    n0=1;
    theta_GaN=asin(sin(theta_i)/n_GaN);
    theta_SiC=asin(sin(theta_i)/n_SiC);
    Z0=377; %Ohm
    Y0=1/Z0*n0/cos(theta_i); %S
    Ys=1/Z0*n_SiC/cos(theta_SiC);
    Y_GaN=1/Z0*n_GaN/cos(theta_GaN);
    k0=2*pi*omega(j); %rad/cm
    k0h_GaN=k0*n_GaN*d_GaN*cos(theta_GaN);
    M=[cos(k0h_GaN) (1i*sin(k0h_GaN))/Y_GaN;
        Y_GaN*1i*sin(k0h_GaN) cos(k0h_GaN)];
    r=(Y0*M(1,1)+Y0*Ys*M(1,2)-M(2,1)-Ys*M(2,2))/(Y0*M(1,1)+Y0*Ys*M(1,2)+M(2,1)+Ys*M(2,2));
    R_TM(j)=abs(r)^2;
end
% plot(omega,R_TM);
% grid on;
% xlabel('frequency(cm^{-1})','FontSize',14);
% ylabel('reflectance','FontSize',14);
% set(gca,'FontSize',14);
% title('Reflectance of GaN sample (670nm,TM)');

% TE+TM
a=0.5;
R=a*R_TE+(1-a)*R_TM;
h=figure(1);
pos=get(gcf,'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100 height*100]); %<- Set size
plot(omega,R,'--','LineWidth',lw);
% grid on;
% xlabel('frequency(cm^{-1})','FontSize',14);
% ylabel('reflectance','FontSize',14);
set(gca,'FontSize',fsz);
% title('Reflectance of GaN film(transfer matrix, 670nm, 50d)');
xlim([400 950]);
hold on;
plot(omega_exp,GaN14_50d,'LineWidth',lw);
% hold off;



%% plot settings

% Here we preserve the size of the image when we save it.
set(gcf,'InvertHardcopy','on');
set(gcf,'PaperUnits', 'inches');
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);

% set legend
hLeg=legend('GaN14 Sim.','GaN14 Exp.','GaN15 Sim.','GaN15 Exp.');
set(hLeg,'Location','Best');
set(hLeg,'FontSize',fsz-6);

% print(h,'TMat_iso','-djpeg','-r150');
