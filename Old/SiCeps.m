function y=SiCeps(omega)
omega_L=970;
omega_T=797;
eps_inf=6.56;
Gamma=4;
y=eps_inf*(1+(omega_L^2-omega_T^2)/(omega_T^2-omega^2-1i*omega*Gamma));
end