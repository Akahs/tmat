% Author: Kaijun Feng
% Note: This model use transfer matrix method and birefringence theory. SiC
% was used as the substrate since the SiC layer is much thicker than GaN
% layer. Both materials are undoped, so a simplified permittivity model was
% used without the plasma term.
%% parameter definition
d_GaN=670e-7; %cm
d_SiC=350e-4; %cm
omega=400:950; %cm-1
c=3e10;%cm/s
f=c*omega;%1/s
N=length(omega);
R_TE=zeros(1,N); %reflectance TE
R_TM=zeros(1,N); %reflectance TM
Gamma_GaN=7; %cm-1 guess
Gamma_SiC=3.5; %cm-1 guess
theta_i=50*pi/180; %50 degree in rad

%% TE, plasma ignored
omega_TO_GaN=560; %cm-1
omega_LO_GaN=746; %cm-1
omega_TO_SiC=797; %cm-1
omega_LO_SiC=970; %cm-1
eps_inf_GaN=5.35;
eps_inf_SiC=6.56;
for j=1:N
    eps_GaN=eps_inf_GaN*(1+(omega_LO_GaN^2-omega_TO_GaN^2)./(omega_TO_GaN^2-...
        omega(j)^2+1i*omega(j)*Gamma_GaN));
    n_GaN=sqrt(eps_GaN);
    eps_SiC=eps_inf_SiC*(1+(omega_LO_SiC^2-omega_TO_SiC^2)./(omega_TO_SiC^2-...
        omega(j)^2+1i*omega(j)*Gamma_SiC));
    n_SiC=sqrt(eps_SiC);
    n0=1;
    theta_GaN=asin(sin(theta_i)/n_GaN);
    theta_SiC=asin(sin(theta_GaN)*n_GaN/n_SiC);
    Z0=377; %Ohm
    Y0=1/Z0*n0*cos(theta_i); %S
    Ys=1/Z0*n_SiC*cos(theta_SiC);
    Y_GaN=1/Z0*n_GaN*cos(theta_GaN);
    k0=2*pi*omega(j); %rad/cm
    k0h_GaN=k0*n_GaN*d_GaN*cos(theta_GaN);
    M=[cos(k0h_GaN) (1i*sin(k0h_GaN))/Y_GaN;
        Y_GaN*1i*sin(k0h_GaN) cos(k0h_GaN)];
    r=(Y0*M(1,1)+Y0*Ys*M(1,2)-M(2,1)-Ys*M(2,2))/(Y0*M(1,1)+Y0*Ys*M(1,2)+M(2,1)+Ys*M(2,2));
    R_TE(j)=abs(r)^2;
end
% plot(omega,R_TE);
% grid on;
% xlabel('frequency(cm^{-1})','FontSize',14);
% ylabel('reflectance','FontSize',14);
% set(gca,'FontSize',14);
% title('Reflectance of GaN sample (670nm)');

%% SiC substrate, TM
omega_TO_GaN_para=533; %cm-1 
omega_LO_GaN_para=740; %cm-1 
omega_TO_SiC_para=788; %cm-1
omega_LO_SiC_para=965; %cm-1
eps_inf_GaN_para=5.35;
eps_inf_SiC_para=6.72;
for j=1:N
    eps_GaN_para=eps_inf_GaN_para*(1+(omega_LO_GaN_para^2-omega_TO_GaN_para^2)/(omega_TO_GaN_para^2-...
        omega(j)^2+1i*omega(j)*Gamma_GaN));
    eps_GaN_perp=eps_inf_GaN*(1+(omega_LO_GaN^2-omega_TO_GaN^2)/(omega_TO_GaN^2-...
        omega(j)^2+1i*omega(j)*Gamma_GaN));
    eps_GaN=eps_GaN_perp+sin(theta_i)^2*(1-eps_GaN_perp/eps_GaN_para);
%     eps_GaN=eps_GaN_para;
    n_GaN=sqrt(eps_GaN);
    eps_SiC_para=eps_inf_SiC_para*(1+(omega_LO_SiC_para^2-omega_TO_SiC_para^2)./(omega_TO_SiC_para^2-...
        omega(j)^2+1i*omega(j)*Gamma_SiC));
    eps_SiC_perp=eps_inf_SiC*(1+(omega_LO_SiC^2-omega_TO_SiC^2)./(omega_TO_SiC^2-...
        omega(j)^2+1i*omega(j)*Gamma_SiC));
    eps_SiC=sin(theta_i)^2+(1-sin(theta_i)^2/eps_SiC_para)*eps_SiC_perp;
    n_SiC=sqrt(eps_SiC);
    n0=1;
    theta_GaN=asin(sin(theta_i)/n_GaN);
    theta_SiC=asin(sin(theta_i)/n_SiC);
    Z0=377; %Ohm
    Y0=1/Z0*n0/cos(theta_i); %S
    Ys=1/Z0*n_SiC/cos(theta_SiC);
    Y_GaN=1/Z0*n_GaN/cos(theta_GaN);
    k0=2*pi*omega(j); %rad/cm
    k0h_GaN=k0*n_GaN*d_GaN*cos(theta_GaN);
    M=[cos(k0h_GaN) (1i*sin(k0h_GaN))/Y_GaN;
        Y_GaN*1i*sin(k0h_GaN) cos(k0h_GaN)];
    r=(Y0*M(1,1)+Y0*Ys*M(1,2)-M(2,1)-Ys*M(2,2))/(Y0*M(1,1)+Y0*Ys*M(1,2)+M(2,1)+Ys*M(2,2));
    R_TM(j)=abs(r)^2;
end
% plot(omega,R_TM);
% grid on;
% xlabel('frequency(cm^{-1})','FontSize',14);
% ylabel('reflectance','FontSize',14);
% set(gca,'FontSize',14);
% title('Reflectance of GaN sample (670nm,TM)');

%% TE+TM
a=0.5;
R=a*R_TE+(1-a)*R_TM;
plot(omega,R,'.-');
hold on;
grid on;
xlabel('frequency(cm^{-1})','FontSize',14);
ylabel('reflectance','FontSize',14);
set(gca,'FontSize',14);
title('Reflectance of GaN film(transfer matrix, 670nm, 50d)');
xlim([400 950]);
plot(omega_exp,GaN14_50d,'r');