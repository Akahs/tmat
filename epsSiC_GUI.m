function y=epsSiC_GUI(omega,handles,part)
omega_L=970;
omega_T=797;
eps_inf=6.56;
if strcmp(part,'film')
    Gamma=str2double(get(handles.edit4,'String'));
else
    Gamma = str2double(get(handles.edit5,'String'));
end
y=eps_inf*(1+(omega_L^2-omega_T^2)/(omega_T^2-omega^2+1i*omega*Gamma));
end