% Author: Kaijun Feng 
% Date: June 15th, 2015
% Note: This function is used to initialize the damping constants for thin
% film and substrate. The variable part can be either 'film' or
% 'substrate'.
function DampingIni(handles,part)
funcs = {'damping_GaN','damping_SiC','damping_InP','damping_Air','damping_silica'};
if strcmp(part,'film')
    id = get(handles.pop1,'Value');
    feval(funcs{id},handles,'film');
elseif strcmp(part,'substrate')
    id = get(handles.pop2,'Value');
    feval(funcs{id},handles,'substrate');
else
    warndlg('Internal error: unknown part name');
end
end