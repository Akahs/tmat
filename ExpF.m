function ExpF(expname,handles)
% Title: ExpF
% Author: Kaijun Feng
% Last edited: 6/4/2015
% Notes: this function can be used to export plots in the form of spectrum
% data.
global omega R;
dlmwrite(expname,[omega' R']);
end